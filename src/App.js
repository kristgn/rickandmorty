import 'bootstrap/dist/css/bootstrap.min.css';

import React from 'react';
import './App.css';
import CharacterList from './components/CharacterList/CharacterList';
import Profile from './components/Profile/Profile.js';
import { Switch, Route } from "react-router-dom";

function App() {
	return (
		<Switch>
			<Route path="/" component={CharacterList} exact />
            <Route path="/profile" component={Profile} />
			<Route component={Error} />
		</Switch>
	);
}

export default App;

