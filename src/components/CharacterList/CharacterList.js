import React from "react";
// Import the CSS file as a module.
import styles from "./CharacterList.module.css";
import { CharacterCard } from "./../Character/Character.js";
import Button from 'react-bootstrap/Button';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';

// Constant To store API url.

class CharacterList extends React.Component {
	// Initialize the State in Class Component.
	constructor(props) {
		super(props);

		this.state = {
			characters: [],
			filteredCharacters: [],
			currentPage: 1,
			url: "https://rickandmortyapi.com/api/character/"
		};
		this.nextPage = this.nextPage.bind(this);
		this.previousPage = this.previousPage.bind(this);
		this.filterList = this.filterList.bind(this);
	}

	// Use ASYNC/AWAIT inside lifecycle method.
	async componentDidMount() {
		try {
			const response = await fetch(this.state.url).then(resp => resp.json());
			// Add the current characters to the chars array.
			let chars = [...this.state.characters];
			// Add the results from the API response.
			chars.push(...response.results);

			// ALWAYS use this.setState() in a Class Method.
			this.setState({
				characters: chars,
				filteredCharacters: chars
			});
		} catch (e) {
			console.error(e);
		}
	}

	nextPage() {
		var nextPage = this.state.currentPage + 1;

		this.setState({
			currentPage: nextPage,
			url: `https://rickandmortyapi.com/api/character/?page=${nextPage}`,
			characters: []
		}, function () {
			this.componentDidMount();
		});
	}

	previousPage() {
		var previousPage = this.state.currentPage - 1;
		this.setState({
			currentPage: previousPage,
			url: `https://rickandmortyapi.com/api/character/?page=${previousPage}`,
			characters: []
		},  function () {
			this.componentDidMount();
		});
	}

	filterList = (event) => {
		let items = this.state.characters;
		items = items.filter((item) => {
			return item.name.toLowerCase().search(event.target.value.toLowerCase()) !== -1;
		});
		this.setState({filteredCharacters: items});
	}

	render() {
		// Create an array of JSX to render
		const filteredCharacters = this.state.filteredCharacters.map(character => {
			// This should render Character components. - Remember the key.
			return (
				<div className={styles.CharacterCard} key={character.id}>
					<CharacterCard props={character}></CharacterCard>
				</div>
			);
		});

		// Render MUST return valid JSX.
		return (
			<div style={{'display': 'flex', 'flexDirection': 'column',
						 'alignItems': 'center', 'paddingTop': '40px'}}>
				<h1>Rick &amp; Morty</h1>
				
				<div>
					<form>
						<input type="text" id="filter" placeholder="Search characters"  onChange={this.filterList}/>
					</form>
				</div>
				
				<div className="Character" style={{'padding': '20', 'display': 'flex',
												   'flexWrap': 'wrap',
												   'justifyContent': 'center'}}>{filteredCharacters}</div>
				
				<div style={{'display': 'flex', 'width': '25%', 'alignItems': 'center',
							 'justifyContent': 'space-evenly', 'padding': '20px'}}>
					<Button variant="primary" onClick={this.previousPage}>Previous</Button>
					<Button variant="primary" onClick={this.nextPage}>Next</Button>
				</div>
			</div>
		);
	}
}

export default CharacterList;
