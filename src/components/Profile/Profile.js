import React from 'react';

class Profile extends React.Component {
    state = {
        character: null
    };

    async componentDidMount() {
        try {
            const response = await fetch(this.props.location.state.url).then(resp => resp.json());

            this.setState({
                character: response
            });
        } catch (e) {
            console.error(e);
        }
    }

    render () {
        if (this.state.character == null) {
            return (
                <div style={{'padding':'100px', 'display': 'flex', 'flexDirection': 'column',
                             'justifyContent': 'center', 'alignItems': 'center'}}>
                    <p>Loading</p>
                </div>
            )
        } else {
            return (
                <div style={{'padding': '30px', 'display': 'flex', 'flexDirection': 'row',
                             'justifyContent': 'center'}}>
                    <div>
                        <img src={this.state.character.image}
                             alt={this.state.character.name} max-width="100%" height="auto" />
                    </div>
                    <div style={{'paddingLeft': '50px', 'display': 'flex', 'flexDirection': 'column'}}>
                        <h1>{this.state.character.name}</h1>
                        <p>Status: {this.state.character.status}</p>
                        <p>Species: {this.state.character.species}</p>
                        <p>Gender: {this.state.character.gender}</p>
                        <p>Origin: {this.state.character.origin.name}</p>
                        <p>Location: {this.state.character.location.name}</p>
                    </div>
                </div>
            )
        }
    }
}

export default Profile;
