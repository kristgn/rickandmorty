import React from 'react';
import Card from 'react-bootstrap/Card';
import { Link } from "react-router-dom";

export const CharacterCard = props => (
    <Link to={{
        pathname: '/profile',
        state: {
            url: props.props.url
        }
    }} style={{ textDecoration: 'none', color: 'inherit' }}>
        <Card className="Card" tag="a">
            <Card.Img variant="top" src={props.props.image} />
            <Card.Body>
                <Card.Title>{props.props.name}</Card.Title>
                <Card.Subtitle className="mb-2 text-muted">{props.props.status}</Card.Subtitle>
                <Card.Text>
                    {props.props.location.name}
                </Card.Text>
            </Card.Body>
        </Card>
    </Link>
);
